# qbin-generator

改编自： [https://gitee.com/renrenio/renren-generator](https://gitee.com/renrenio/renren-generator?_from=gitee_search)
直接生成一个可运行的单表增删改查前后台

步骤：

```html
1.修改generator.properties的基本配置(包名等)

2.修改application.yml的数据库配置

3.启动代码生成器，前往http://localhost:8888

4.选择数据表后下载代码

5.解压后修改application.yml启动即可

6.前往http://localhost:8080/modules/模块名/数据库名.html(看templates文件夹就知道了)
```

