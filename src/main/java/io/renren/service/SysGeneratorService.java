/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.service;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import io.renren.dao.GeneratorDao;
import io.renren.utils.GenUtils;
import io.renren.utils.PageUtils;
import io.renren.utils.Query;
import lombok.extern.slf4j.Slf4j;

/**
 * 代码生成器
 *
 * @author Mark sunlightcs@gmail.com
 * @author qiangbin
 */
@Slf4j
@Service
public class SysGeneratorService {
	@Autowired
	private GeneratorDao generatorDao;

	/**
	 * 查所有表，可模糊查询
	 * 
	 * @param query
	 * @return
	 * @date 2020年5月28日
	 */
	public PageUtils queryList(Query query) {
		Page<?> page = PageHelper.startPage(query.getPage(), query.getLimit());
		List<Map<String, Object>> list = generatorDao.queryList(query);
		log.info("{}", list);
		return new PageUtils(list, (int) page.getTotal(), query.getLimit(), query.getPage());
	}

	/**
	 * 单个表
	 * 
	 * @param tableName
	 * @return
	 * @date 2020年5月28日
	 */
	public Map<String, String> queryTable(String tableName) {
		return generatorDao.queryTable(tableName);
	}

	/**
	 * 表coloumn
	 * 
	 * @param tableName
	 * @return
	 * @date 2020年5月28日
	 */
	public List<Map<String, String>> queryColumns(String tableName) {
		return generatorDao.queryColumns(tableName);
	}

	/**
	 * 生成代码
	 * 
	 * @param tableNames
	 * @return
	 * @date 2020年5月28日
	 */
	public byte[] generatorCode(String[] tableNames) {
		log.info("Export tables :=====" + Arrays.toString(tableNames) + "=====");
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);

		//
		for (String tableName : tableNames) {
			// 查询表信息
			Map<String, String> table = queryTable(tableName);
			System.out.println("table info: \t" + table.toString());
			// 查询列信息
			List<Map<String, String>> columns = queryColumns(tableName);
			System.out.println("columns info: \t" + columns.toString());

			// 生成该表的代码
			GenUtils.generatorCode(table, columns, zip);
		}
		GenUtils.clearCache();
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}
}
