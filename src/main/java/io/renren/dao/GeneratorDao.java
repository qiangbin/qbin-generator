/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.dao;

import java.util.List;
import java.util.Map;

/**
 * 数据库接口
 *
 * @author Mark sunlightcs@gmail.com
 * @author qiangbin
 */
public interface GeneratorDao {
	/**
	 * 查所有表
	 * 
	 * @param map 添加tableName可模糊查询
	 * @return
	 * @date 2020年5月28日
	 */
	List<Map<String, Object>> queryList(Map<String, Object> map);

	/**
	 * 查单个表
	 * 
	 * @param tableName
	 * @return
	 * @date 2020年5月28日
	 */
	Map<String, String> queryTable(String tableName);

	/**
	 * 查单个表的字段
	 * 
	 * @param tableName
	 * @return
	 * @date 2020年5月28日
	 */
	List<Map<String, String>> queryColumns(String tableName);
}
