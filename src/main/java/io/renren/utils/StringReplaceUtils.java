package io.renren.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 文件字符串替换工具
 * 
 * @Description:
 * @author qiangbin
 * @date 2019年12月13日
 * @version v0.0
 */
public class StringReplaceUtils {

	/**
	 * <pre>
	 * 	替换文件夹下所有文件的 某一字符串到 新字符串
	 * 	文件字符串替换工具，使用默认文件过滤集合
	 * </pre>
	 * 
	 * <pre>
	 * 	.txt
	 * 	.java
	 * 	.js
	 * 	.html
	 * </pre>
	 * 
	 * @Description:
	 * @param
	 * @return
	 * @date 2019年12月13日
	 * @version v0.0
	 */
	public static void replaceStringToAnotherString(File f, String oldString, String newString) {
		List<String> fileSufixList = new LinkedList<String>();
		fileSufixList.add(".txt");
		fileSufixList.add(".java");
		fileSufixList.add(".js");
		fileSufixList.add(".html");
		replaceStringToAnotherString(f, oldString, newString, fileSufixList);
	}

	/**
	 * <pre>
	 * 	替换文件夹下所有文件的 某一字符串到 新字符串
	 * 	文件字符串替换工具
	 * </pre>
	 * 
	 * @Description:
	 * @param fileSufixList 文件后缀名集合
	 * @return
	 * @date 2019年12月13日
	 * @version v0.0
	 */
	@SuppressWarnings("unchecked")
	public static void replaceStringToAnotherString(File f, String oldString, String newString,
			List<String> fileSufixList) {
		if (!f.exists())
			return;
		if (fileSufixList == null)
			fileSufixList = Collections.EMPTY_LIST;

		if (f.isDirectory()) { // 目录的话

			File[] listFiles = f.listFiles();
			for (File file : listFiles) {
				replaceStringToAnotherString(file, oldString, newString, fileSufixList);// 子文件
			}
		} else {
			int index = f.getName().lastIndexOf(".");
			String fileSufix = f.getName().substring(index);
			if (fileSufixList.contains(fileSufix))
				alterStringToCreateNewFile(f, oldString, newString);
		}
	}

	/**
	 * 替换文件的字符串到新字符串
	 * 
	 * @Description:
	 * @param
	 * @return
	 * @date 2019年12月13日
	 * @version v0.0
	 */
	public static void alterStringToCreateNewFile(File f, String oldString, String newString) {
		try {
			long start = System.currentTimeMillis(); // 开始时间
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f))); // 创建对目标文件读取流
			File newFile = new File("src/test/" + f.getName()); // 创建临时文件
			if (!newFile.exists()) {
				newFile.createNewFile(); // 不存在则创建
			}
			// 创建对临时文件输出流，并追加
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile, true)));
			String string = null; // 存储对目标文件读取的内容
			int sum = 0; // 替换次数
			while ((string = br.readLine()) != null) {
				// 判断读取的内容是否包含原字符串
				if (string.contains(oldString)) {
					// 替换读取内容中的原字符串为新字符串
					string = new String(string.replace(oldString, newString));
					sum++;
				}
				bw.write(string);
				bw.newLine(); // 添加换行
			}
			br.close(); // 关闭流，对文件进行删除等操作需先关闭文件流操作
			bw.close();
			String filePath = f.getPath();
			f.delete(); // 删除源文件
			newFile.renameTo(new File(filePath)); // 将新文件更名为源文件
			long time = System.currentTimeMillis() - start; // 整个操作所用时间;
			if (sum > 0)
				System.out.println(f.getAbsolutePath() + "\t\t" + sum + "个" + oldString + "替换成" + newString + "耗费时间:"
						+ time + "ms");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
