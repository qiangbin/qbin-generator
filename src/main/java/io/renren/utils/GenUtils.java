/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import io.renren.entity.ColumnEntity;
import io.renren.entity.TableEntity;
import lombok.extern.slf4j.Slf4j;

/**
 * 代码生成器 工具类
 *
 * @author Mark sunlightcs@gmail.com
 * @author sqb
 */
@Slf4j
public class GenUtils {

	/**
	 * 模板
	 * 
	 * @Description:
	 * @return
	 * @date 2020年5月28日
	 * @version v0.0
	 */
	public static List<String> getTemplates() {
		List<String> templates = new ArrayList<String>();
		templates.add("template/Entity.java.vm");
		templates.add("template/Dao.java.vm");
		templates.add("template/Dao.xml.vm");
		templates.add("template/Service.java.vm");
		templates.add("template/ServiceImpl.java.vm");
		templates.add("template/Controller.java.vm");
		templates.add("template/list.html.vm");
		templates.add("template/list.js.vm");

		templates.add("template/Constant.java.vm");
		templates.add("template/PageUtils.java.vm");
		templates.add("template/Query.java.vm");
		templates.add("template/R.java.vm");
		templates.add("template/SQLFilter.java.vm");
		templates.add("template/GeneratorApplication.java.vm");

		return templates;
	}

	/**
	 * 
	 * 
	 * @Description:
	 * @param table   表信息
	 * @param columns 类信息list
	 * @param zip     输出的zip
	 * @date 2020年5月28日
	 * @version v0.0
	 */
	public static void generatorCode(Map<String, String> table, List<Map<String, String>> columns,
			ZipOutputStream zip) {
		Configuration config = getConfig(); // 配置信息
		TableEntity tableEntity = new TableEntity();// 表信息
		tableEntity.setTableName(table.get("tableName"));
		tableEntity.setComments(table.get("tableComment") != null ? table.get("tableComment") : table.get("tableName"));
		// 表名转换成Java类名,去掉prefix
		String className = tableToJava(tableEntity.getTableName(), config.getString("tablePrefix"));
		tableEntity.setClassName(className);
		tableEntity.setClassname(StringUtils.uncapitalize(className));

		// 列信息
		List<ColumnEntity> columsList = new ArrayList<>();
		// 提取column
		boolean hasBigDecimal = false;
		for (Map<String, String> column : columns) {

			ColumnEntity columnEntity = new ColumnEntity();
			columnEntity.setColumnName(column.get("columnName"));// 列名
			columnEntity.setDataType(column.get("dataType"));// 列类型
			columnEntity.setComments(StringUtils.isBlank(column.get("columnComment")) ? column.get("columnName")
					: column.get("columnComment"));// 注释
			columnEntity.setExtra(column.get("extra"));// auto_increment 等

			// 列名转换成Java属性名
			String attrName = columnToJava(columnEntity.getColumnName());
			columnEntity.setAttrName(attrName);
			columnEntity.setAttrname(StringUtils.uncapitalize(attrName));

			// 列的数据类型，转换成Java类型
			String attrType = config.getString(columnEntity.getDataType(), "unknowType(不找到什么类型)");
			columnEntity.setAttrType(attrType);
			if (!hasBigDecimal && attrType.equals("BigDecimal")) {
				hasBigDecimal = true;
			}
			// 是否主键
			if ("PRI".equalsIgnoreCase(column.get("columnKey")) && tableEntity.getPk() == null) {
				tableEntity.setPk(columnEntity);
			}

			columsList.add(columnEntity);
		}
		tableEntity.setColumns(columsList);

		// 没主键，则第一个字段为主键
		if (tableEntity.getPk() == null) {
			tableEntity.setPk(tableEntity.getColumns()
					.get(0));
		}

		// 设置velocity资源加载器
		Properties prop = new Properties();
		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.init(prop);

		// 封装模板数据
		Map<String, Object> map = new HashMap<>();
		initVelocityParams(config, tableEntity, map);
		map.put("hasBigDecimal", hasBigDecimal);
		VelocityContext context = new VelocityContext(map);

		// 获取模板列表
		List<String> templates = getTemplates();
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);

			try {

				String fileName = getFileName(// name
						template, //
						tableEntity.getClassName(), //
						config.getString("package"), //
						config.getString("moduleName")//
				);
				// 添加到zip
				if (!fileNameCache.contains(fileName)) {
					zip.putNextEntry(new ZipEntry(fileName));
					fileNameCache.add(fileName);
					IOUtils.write(sw.toString(), zip, "UTF-8");// file
					zip.closeEntry();
				}
				IOUtils.closeQuietly(sw);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RRException("渲染模板失败，表名：" + tableEntity.getTableName(), e);
			}
		}
		try {
			// 复制静态文件
			commonsFileCopyToResource(COMMON_FILE, zip);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RRException("资源加载失败", e);
		}
	}

	/**
	 * 添加Velocity参数
	 * 
	 * @param config
	 * @param tableEntity
	 * @param map
	 * @date 2020年5月28日
	 */
	private static void initVelocityParams(Configuration config, TableEntity tableEntity, Map<String, Object> map) {
		// 包
		String mainPath = config.getString("mainPath");
		mainPath = StringUtils.isBlank(mainPath) ? "s.q.b" : mainPath;
		map.put("tableName", tableEntity.getTableName());
		map.put("comments", StringUtils.isBlank(tableEntity.getComments()) ? tableEntity.getTableName()
				: tableEntity.getComments());
		map.put("pk", tableEntity.getPk());
		map.put("className", tableEntity.getClassName());
		map.put("classname", tableEntity.getClassname());
		map.put("pathName", tableEntity.getClassname());
		map.put("columns", tableEntity.getColumns());

		map.put("package", config.getString("package"));
		map.put("moduleName", config.getString("moduleName"));
		map.put("author", config.getString("author"));
		map.put("email", config.getString("email"));
		map.put("search", config.getString("search"));
		map.put("datetime", DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
		map.put("mainPath", mainPath);
	}

	/** 静态文件路径 */
	public static String FILE_PATH;
	/** 静态文件 */
	public static File COMMON_FILE;
	// 过滤已经添加的文件
	private static Set<String> fileNameCache = new HashSet<>();

	/**
	 * 清空
	 * 
	 * @author qbin
	 * @date 2020/06/06
	 */
	public static void clearCache() {
		fileNameCache.clear();
	}

	/**
	 * 用于加载静态文件
	 */
	static {
		try {
			COMMON_FILE = new File(GenUtils.class.getClassLoader()
					.getResource("gen-static")
					.toURI());
			FILE_PATH = COMMON_FILE.getAbsolutePath();
		} catch (URISyntaxException e) {
			log.error(e.getLocalizedMessage());
		}
	}

	/**
	 * 递归复制文件
	 * 
	 * <pre>
	 * 	----pom.xml
	 * 		|
	 * 		src/
	 * 			--main/
	 * 				|
	 * 				 --java/
	 * 				|
	 * 				 --resources/
	 * </pre>
	 * 
	 * @Description:
	 * @param f
	 * @param zip
	 * @throws IOException
	 * @date 2020年5月28日
	 * @version v0.0
	 */
	private static void commonsFileCopyToResource(File f, ZipOutputStream zip) throws IOException {

		if (f.isDirectory()) {
			// 目录的话递归复制
			File[] listFiles = f.listFiles();
			for (File file : listFiles) {
				// 递归
				commonsFileCopyToResource(file, zip);
			}
			return;
		}

		// ---------文件复制---------
		// 忽略readme
		if (f.getName()
				.equals("README.MD")) {
			return;
		}
		// 过滤已经添加
		if (fileNameCache.contains(f.getAbsolutePath())) {
			return;
		}
		FileInputStream fileInputStream = new FileInputStream(f);
		// 处理gitignore
		if (f.getName()
				.equals("gitignore")) {
			// 复制文件
			zip.putNextEntry(new ZipEntry("." + f.getAbsolutePath()
					.replace(FILE_PATH + File.separator, "")));
			fileNameCache.add(f.getAbsolutePath());
			IOUtils.copy(fileInputStream, zip);
			zip.closeEntry();
			return;
		}
		// 复制文件
		zip.putNextEntry(new ZipEntry(f.getAbsolutePath()
				.replace(FILE_PATH + File.separator, "")));
		fileNameCache.add(f.getAbsolutePath());
		IOUtils.copy(fileInputStream, zip);
		zip.closeEntry();
		fileInputStream.close();
	}

	/**
	 * 表名转换
	 * 
	 * @param tableName   表名
	 * @param tablePrefix 表前缀
	 * @return java类名
	 * @date 2020年5月28日
	 */
	public static String tableToJava(String tableName, String tablePrefix) {
		if (StringUtils.isNotBlank(tablePrefix)) {
			tableName = tableName.replace(tablePrefix, "");
		}
		return columnToJava(tableName);
	}

	/**
	 * 列名转换成Java属性名(下划线，驼峰)
	 * 
	 * @param columnName xx_yy
	 * @return XxYy
	 * @date 2020年5月28日
	 */
	public static String columnToJava(String columnName) {
		return WordUtils.capitalizeFully(columnName, new char[] { '_' })
				.replace("_", "");
	}

	/**
	 * 获取generator.properties配置信息
	 * 
	 * @return
	 * @author qbin
	 * @date 2020/06/06
	 */
	public static Configuration getConfig() {
		try {
			return new PropertiesConfiguration("generator.properties");
		} catch (ConfigurationException e) {
			throw new RRException("获取配置文件失败，", e);
		}
	}

	/**
	 * 获取在zip的位置，文件名
	 * 
	 * @param template
	 * @param className
	 * @param packageName
	 * @param moduleName
	 * @return
	 * @author qbin
	 * @date 2020/06/06
	 */
	public static String getFileName(String template, String className, String packageName, String moduleName) {
		String packagePath = "src" + File.separator + "main" + File.separator + "java" + File.separator;
		if (StringUtils.isNotBlank(packageName)) {
			packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
		}

		if (template.contains("Entity.java.vm")) {
			return packagePath + "entity" + File.separator + className + "Entity.java";
		}

		if (template.contains("Dao.java.vm")) {
			return packagePath + "dao" + File.separator + className + "Dao.java";
		}

		if (template.contains("Service.java.vm")) {
			return packagePath + "service" + File.separator + className + "Service.java";
		}

		if (template.contains("ServiceImpl.java.vm")) {
			return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
		}

		if (template.contains("Controller.java.vm")) {
			return packagePath + "controller" + File.separator + className + "Controller.java";
		}

		if (template.contains("Dao.xml.vm")) {
			return "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mapper"
					+ File.separator + moduleName + File.separator + className + "Dao.xml";
		}

		if (template.contains("list.html.vm")) {
			return "src" + File.separator + "main" + File.separator + "resources" + File.separator + "templates"
					+ File.separator + "modules" + File.separator + moduleName + File.separator
					+ className.toLowerCase() + ".html";
		}

		if (template.contains("list.js.vm")) {
			return "src" + File.separator + "main" + File.separator + "resources" + File.separator + "static"
					+ File.separator + "js" + File.separator + "modules" + File.separator + moduleName + File.separator
					+ className.toLowerCase() + ".js";
		}

		if (template.contains("menu.sql.vm")) {
			return className.toLowerCase() + "_menu.sql";
		}

		if (template.contains("Constant.java.vm") || template.contains("template/MybatisPlusConfig.java.vm")
				|| template.contains("template/MyUtil.java.vm") || template.contains("template/PageUtils.java.vm")
				|| template.contains("template/Query.java.vm") || template.contains("template/R.java.vm")
				|| template.contains("template/SQLFilter.java.vm")) {
			String name = template.substring(template.indexOf('/') + 1, template.lastIndexOf('.'));
			return packagePath + "utils" + File.separator + name;
		}
		if (template.contains("template/GeneratorApplication.java.vm")) {
			String name = template.substring(template.indexOf('/') + 1, template.lastIndexOf('.'));
			return packagePath + File.separator + name;
		}

		return null;
	}
}
